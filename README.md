Here is some base text
- [ ] One
- [ ] 

<img align="left" width="300" height="200" src="https://gitea.com/assets/img/logo.svg">

- [ ] Two
- [ ] Three
- [ ] Four
- [ ] 

More text


#### dense list
- [ ] foo
- [ ] barbarbarbarbarbarbarbar barbarbarbarbarbarbarbar barbarbarbarbarbarbarbar barbarbarbarbarbarbar barbarbarbarbar
- [ ] baz

#### not so dense list
- [ ] foo

- [ ] barbarbarbarbarbarbarbar barbarbarbarbarbarbarbar barbarbarbarbarbarbarbar barbarbarbarbarbarbar barbarbarbarbar
- [ ] second item

- [ ] baz